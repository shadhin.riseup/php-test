<?php

namespace App\Core;

use Exception;
use PDO;
use PDOException;
use Throwable;
use function PHPUnit\Framework\throwException;

class Database
{
    private $pdo;
    private $table;
    private $select = '*';
    private $where = '';
    private $orWhere = '';
    private $orderBy = '';
    private $limit;
    private $offset;
    private $query;

    public function __construct($host, $dbName, $userName, $password)
    {
        try {
            $this->pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $userName, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new Exception('Failed to connect to database: ' . $e->getMessage());
        }
    }

    public function table($tableName)
    {
        $this->table = $tableName;
        return $this;
    }

    public function select($select)
    {
        $this->select = $select;
        return $this;
    }

    public function where(...$args)
    {
        // Extract arguments
        $columnName = $args[0];
        $operator = isset($args[2]) ? $args[1] : '=';
        $value = isset($args[2]) ? $args[2] : $args[1];

        // Prepare the WHERE clause
        // Add parameterized values to prevent SQL injection
        // You can also validate column names and operators here
        $this->where .= (empty($this->where) ? '' : ' AND ') . $columnName . ' ' . $operator . ' ?';
        $this->query['values'][] = $value;

        return $this;
    }

    public function orWhere(...$args)
    {
        // Extract arguments
        $columnName = $args[0];
        $operator = isset($args[2]) ? $args[1] : '=';
        $value = isset($args[2]) ? $args[2] : $args[1];

        // Prepare the orWhere clause
        // Add parameterized values to prevent SQL injection
        // You can also validate column names and operators here
        $this->orWhere .= empty($this->where) ? 'WHERE' : (empty($this->orWhere) ? '' : ' OR ') . $columnName . ' ' . $operator . ' ?';
        $this->query['values'][] = $value;

        return $this;
    }

    public function orderBy($columnName, $orderType = 'asc')
    {
        $this->orderBy .= 'ORDER BY ' . $columnName . ' ' . $orderType;
        return $this;
    }

    public function orderByAsc($columnName)
    {
        $this->orderBy .= 'ORDER BY ' . $columnName . ' asc';
        return $this;
    }

    public function orderByDesc($columnName)
    {
        $this->orderBy .= 'ORDER BY ' . $columnName . ' desc';
        return $this;
    }

    public function limit($limit = 1)
    {
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset = 0)
    {
        $this->offset = $offset;
        return $this;
    }

    public function find($id)
    {
        return $this->where(['id', $id]);
    }

    public function first()
    {
        $this->setLimit();
        $sql =  $this->getQuery();
        $sql->execute();
        return $sql->fetchAll(PDO::FETCH_OBJ);
    }

    public function get()
    {
        $sql =  $this->getQuery();
        $sql->execute();
        return $sql->fetchAll(PDO::FETCH_OBJ);
    }

    public function toSql()
    {
        $sql = $this->getQuery();
        return $sql->queryString;
    }

    private function getQuery()
    {
        $this->setQuery();
        $sql = $this->pdo->prepare($this->query['sql']);
        $sql = $this->bindValues($sql);
        return $sql;
    }

    private function bindValues($sql)
    {
        // Bind parameters
        foreach ($this->query['values'] as $key => $value) {
            // PDO parameters are 1-indexed, so add 1 to $key
            $sql->bindValue($key + 1, $value);
        }
        return $sql;
    }

    private function setQuery()
    {
        $whereQuery = strlen($this->where) ? sprintf('WHERE %s', $this->where) : '';
        $orWhereQuery = strlen($this->orWhere) ? sprintf('OR %s', $this->orWhere) : '';
        $limitQuery = isset($this->limit) ? sprintf('LIMIT %s', $this->limit) : '';
        $offsetQuery = isset($this->offset) ? sprintf('OFFSET %s', $this->offset) : '';

        $query = sprintf('SELECT %s FROM %s %s %s %s %s %s', $this->select, $this->table, $whereQuery, $orWhereQuery, $this->orderBy, $limitQuery, $offsetQuery);
        $this->query['sql'] = $query;
    }

    private function setOffset($offset = 0)
    {
        $this->offset($offset);
    }

    private function setLimit($limit = 1)
    {
        $this->limit($limit);
    }
}
